package ru.t1.panasyuk.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void log(@NotNull String message);

}