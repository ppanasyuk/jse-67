package ru.panasyuk.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

//    @NotNull
//    String getActiveMqHost();
//
//    @NotNull
//    String getApplicationConfig();
//
//    @NotNull
//    String getApplicationLog();
//
//    @NotNull
//    String getApplicationName();
//
//    @NotNull
//    String getApplicationVersion();
//
//    @NotNull
//    String getAuthorEmail();
//
//    @NotNull
//    String getAuthorName();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBHbm2DdlAuto();

    @NotNull
    String getDBFormatSql();

    @NotNull
    String getDBCacheRegionFactory();

    @NotNull
    String getDBCacheRegionPrefix();

    @NotNull
    String getDBConfigFilePath();

    @NotNull
    String getDBUseQueryCache();

    @NotNull
    String getDBUseMinimalPuts();

    @NotNull
    String getDBLazyLoadNoTransEnabled();

    @NotNull
    Boolean getDBLoggingEnabled();

    @NotNull
    String getDBSecondLevelCacheEnabled();

    @NotNull
    String getDBUser();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUrl();

//    @NotNull
//    String getGitBranch();
//
//    @NotNull
//    String getGitCommitId();
//
//    @NotNull
//    String getGitCommitterName();
//
//    @NotNull
//    String getGitCommitterEmail();
//
//    @NotNull
//    String getGitCommitMessage();
//
//    @NotNull
//    String getGitCommitTime();
//
//    @NotNull
//    String getServerPort();
//
//    @NotNull
//    String getServerHost();
//
//    @NotNull
//    String getSessionKey();
//
//    int getSessionTimeout();

}