package ru.panasyuk.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.panasyuk.tm.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {
}