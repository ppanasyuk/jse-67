package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.tm.api.endpoint.ITaskEndpoint;
import ru.panasyuk.tm.api.service.ITaskService;
import ru.panasyuk.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.panasyuk.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @NotNull
    @WebMethod
    @PostMapping(value = "/save", produces = "application/json")
    public Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull final Task task
    ) {
        return taskService.save(task);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public void saveAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody @NotNull final List<Task> tasks
    ) {
        taskService.saveAll(tasks);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existById/{id}")
    public boolean existById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return taskService.existById(id);
    }

    @Override
    @WebMethod
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return taskService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        taskService.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody @NotNull final Task task
    ) {
        taskService.delete(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody @NotNull final List<Task> tasks
    ) {
        taskService.clear(tasks);
    }

    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        taskService.clear();
    }

}