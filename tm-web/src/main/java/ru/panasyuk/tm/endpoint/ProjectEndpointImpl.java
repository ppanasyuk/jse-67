package ru.panasyuk.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.tm.api.endpoint.IProjectEndpoint;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.panasyuk.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @NotNull
    @WebMethod
    @PostMapping(value = "/save", produces = "application/json")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull final Project project
    ) {
        return projectService.save(project);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public void saveAll(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody @NotNull final List<Project> projects
    ) {
        projectService.saveAll(projects);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existById/{id}")
    public boolean existById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return projectService.existById(id);
    }

    @Override
    @WebMethod
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long count() {
        return projectService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        projectService.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull final Project project
    ) {
        projectService.delete(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody @NotNull final List<Project> projects
    ) {
        projectService.clear(projects);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        projectService.clear();
    }

}