package ru.panasyuk.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.panasyuk.tm.client.ProjectRestEndpointClient;
import ru.panasyuk.tm.client.ProjectsRestEndpointClient;
import ru.panasyuk.tm.marker.IntegrationCategory;
import ru.panasyuk.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    final ProjectsRestEndpointClient projectsClient = ProjectsRestEndpointClient.client();

    @NotNull
    final List<Project> projects = new ArrayList<>();

    @Before
    public void init() {
        for (int i = 0; i < 4; i++) {
            @NotNull final Project project = new Project("Project " + i);
            projects.add(project);
            client.save(project);
        }
    }

    @After
    public void afterTest() {
        for (@NotNull final Project project : projects) {
            client.delete(project);
        }
        projects.clear();
    }

    @Test
    public void testSave() {
        final long projectSize = projectsClient.count();
        final long expectedSize = projectSize + 1;
        @NotNull final Project projectTest = new Project("Project Test");
        @Nullable final Project savedProject = client.save(projectTest);
        Assert.assertNotNull(savedProject);
        final long actualSize = projectsClient.count();
        Assert.assertEquals(expectedSize, actualSize);
        client.delete(projectTest);
    }

    @Test
    public void testFindById() {
        for (@NotNull final Project project : projects) {
            @Nullable final Project foundProject = client.findById(project.getId());
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void testDeleteById() {
        for (@NotNull final Project project : projects) {
            client.deleteById(project.getId());
            @Nullable final Project deletedProject = client.findById(project.getId());
            Assert.assertNull(deletedProject);
        }
    }

    @Test
    public void testDelete() {
        for (@NotNull final Project project : projects) {
            client.delete(project);
            @Nullable final Project deletedProject = client.findById(project.getId());
            Assert.assertNull(deletedProject);
        }
    }

}