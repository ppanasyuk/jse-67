package ru.t1.panasyuk.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.dto.model.UserDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface UserDtoRepository extends AbstractDtoRepository<UserDTO> {

    @NotNull
    UserDTO findFirstByLogin(@Nullable String login);

    @NotNull
    UserDTO findFirstByEmail(@Nullable String email);

    @Nullable
    @Query("FROM UserDTO m ORDER BY m.created DESC")
    List<UserDTO> findByIndex(@NotNull Pageable pageable);

}