package ru.t1.panasyuk.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IDtoService<M extends AbstractModelDTO> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    void clear();

    boolean existsById(@Nullable String id);

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    long getSize();

    @Nullable
    M remove(@NotNull M model);

    M removeById(@Nullable String id);

    M removeByIndex(@Nullable Integer index);

    void update(@NotNull M model);

}