package ru.t1.panasyuk.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.model.Session;

import java.util.List;

public interface ISessionService extends IUserOwnedService<Session> {

    Session remove(@Nullable Session session);

    List<Session> findAll();

}
