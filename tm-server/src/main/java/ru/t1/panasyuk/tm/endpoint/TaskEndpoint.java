package ru.t1.panasyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.t1.panasyuk.tm.api.endpoint.ITaskEndpoint;
import ru.t1.panasyuk.tm.dto.request.task.*;
import ru.t1.panasyuk.tm.dto.response.task.*;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.panasyuk.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDTO task;
        try {
            task = getProjectTaskDtoService().bindTaskToProject(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            return new TaskBindToProjectResponse(e);
        }
        return new TaskBindToProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().changeTaskStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            return new TaskChangeStatusByIdResponse(e);
        }
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().changeTaskStatusByIndex(userId, index, status);
        } catch (@NotNull final Exception e) {
            return new TaskChangeStatusByIndexResponse(e);
        }
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            getTaskService().clear(userId);
        } catch (@NotNull final Exception e) {
            return new TaskClearResponse(e);
        }
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().changeTaskStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            return new TaskCompleteByIdResponse(e);
        }
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            return new TaskCompleteByIndexResponse(e);
        }
        return new TaskCompleteByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().create(userId, name, description);
        } catch (@NotNull final Exception e) {
            return new TaskCreateResponse(e);
        }
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskFindAllByProjectIdResponse findAllByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskFindAllByProjectIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<TaskDTO> tasks;
        try {
            tasks = getTaskDtoService().findAllByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            return new TaskFindAllByProjectIdResponse(e);
        }
        return new TaskFindAllByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskFindOneByIdResponse findTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskFindOneByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            return new TaskFindOneByIdResponse(e);
        }
        return new TaskFindOneByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskFindOneByIndexResponse findTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskFindOneByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().findOneByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            return new TaskFindOneByIndexResponse(e);
        }
        return new TaskFindOneByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<TaskDTO> tasks;
        try {
            tasks = getTaskDtoService().findAll(userId, sort);
        } catch (@NotNull final Exception e) {
            return new TaskListResponse(e);
        }
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task;
        @Nullable final TaskDTO taskDTO;
        try {
            taskDTO = getTaskDtoService().findOneById(userId, id);
            task = getTaskService().removeById(userId, id);
        } catch (@NotNull final Exception e) {
            return new TaskRemoveByIdResponse(e);
        }
        return new TaskRemoveByIdResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task;
        @Nullable final TaskDTO taskDTO;
        try {
            taskDTO = getTaskDtoService().findOneByIndex(userId, index);
            task = getTaskService().removeByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            return new TaskRemoveByIndexResponse(e);
        }
        return new TaskRemoveByIndexResponse(taskDTO);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            return new TaskStartByIdResponse(e);
        }
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIndexResponse startTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            return new TaskStartByIndexResponse(e);
        }
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final TaskDTO task;
        try {
            task = getProjectTaskDtoService().unbindTaskFromProject(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            return new TaskUnbindFromProjectResponse(e);
        }
        return new TaskUnbindFromProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            return new TaskUpdateByIdResponse(e);
        }
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task;
        try {
            task = getTaskDtoService().updateByIndex(userId, index, name, description);
        } catch (@NotNull final Exception e) {
            return new TaskUpdateByIndexResponse(e);
        }
        return new TaskUpdateByIndexResponse(task);
    }

}
