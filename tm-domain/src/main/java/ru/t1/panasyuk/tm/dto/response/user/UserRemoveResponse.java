package ru.t1.panasyuk.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserRemoveResponse extends AbstractUserResponse {

    @Nullable
    private UserDTO user;

    public UserRemoveResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    public UserRemoveResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}