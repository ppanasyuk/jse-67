package ru.t1.panasyuk.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public TaskRemoveByIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

    public TaskRemoveByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token);
        this.index = index;
    }

}